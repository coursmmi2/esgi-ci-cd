FROM composer:2 AS builder
 
# Add our application files here
WORKDIR /app
COPY . .

# Install deps
RUN composer install --no-dev --optimize-autoloader

# this is the REAL application container now
FROM alpine

RUN apk add --update \
--repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
--repository http://dl-cdn.alpinelinux.org/alpine/edge/community \
 php7 php7-fpm php7-opcache nginx curl supervisor \ 
 php7-xml php7-iconv php7-xmlreader php-json \
 php7-ctype php7-session && \
 rm /etc/nginx/conf.d/default.conf
 
# we don't need to do anything here by copy the `/app` folder from the
# `deps` stage above. Its /app folder will have all the vendor files etc
COPY --chown=nobody --from=builder /app/ /var/www/app/
COPY nginx/nginx.conf /etc/nginx/nginx.conf

COPY nginx/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY nginx/php.ini /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY nginx/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN chown -R nobody.nobody /var/www/app && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx

USER nobody
 
EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping