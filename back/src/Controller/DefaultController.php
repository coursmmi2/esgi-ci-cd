<?php

declare(strict_types=1);

namespace App\Controller;

use App\Document\Todolist;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController
{
//     public function index(): JsonResponse
//     {
//         $todolist = new Todolist();
//
//
//         return new JsonResponse([
//             [
//                 'id' => 1,
//                 'info' => 'test 1 qwerty base',
//                 'completed' => false,
//             ],
//         ], 200, ['Access-Control-Allow-Origin' => '*']);
//     }

    public function index(DocumentManager $dm)
    {

//        $todolist = new Todolist();
//        $todolist->setTitle('A Foo Bar');
//        $todolist->setCompleted(false);
//
//        $dm->persist($todolist);
//        $dm->flush();

        $repository = $dm->getRepository(Todolist::class)->find('5f5b800b8a389573f83c8f82');
        return new JsonResponse([
                [
                    'id' => $repository->id,
                    'title' => $repository->title,
                    'completed' => false,
                ]
        ], 200, ['Access-Control-Allow-Origin' => '*']);

    }
}

